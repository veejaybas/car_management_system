import datetime as dm
from datetime import timedelta
import logging
import pytz
logging.basicConfig(level=logging.DEBUG)

IST = pytz.timezone('Asia/Kolkata')
print('Car Management System')
class Cars:
    def __init__(self):
        self._brand = ''
        self._model = ''
        self._pub_year = 0
        self._color = ''
        self._mileage = 0
        self._assigned = ''
        self._assigned_by = ''
    # Add new Vehicle here #
    def addVehicle(self):
        try:
            self._brand = input('Enter vehicle brand: ')
            self._model = input('Enter vehicle model: ')
            self._pub_year = int(input('Enter vehicle Publish year: '))
            self._color = input('Enter vehicle color: ')
            self._mileage = int(input('Enter vehicle mileage: '))
            self._assigned = str(input('Assigned ? True or False? : '))
            if self._assigned.lower() not in ["true","false" ]:
                logging.info("Tried for Assigned : " +self._assigned )
                print('Invalid Input Assigned must True or False')
                return False
            if self._assigned.lower() == 'true':
                self._assigned = dm.datetime.now(IST).strftime('%d-%m-%Y %H:%M:%S')
                self._assigned_by = 'M'
                return True
            self._assigned = ''
            self._assigned_by = ''
            return True
        except ValueError:
            logging.error("While Adding vechicle : " +str(ValueError) )
            print('Please check entering numbers for mileage and year')
            return False
        
    def __str__(self):
        return '\t'.join(str(x) for x in [self._brand, self._model, self._pub_year, self._color, self._mileage,self._assigned_by,self._assigned])

class WareHouse:
    # Cars list are stored here #
    def __init__(self):
        self.vehicles = []
        # Default mx duration 120 minutes #
        self.default_assigned_duration = 120
    def addVehicle(self):
        vehicle = Cars()
        if vehicle.addVehicle() == True:
            # vehicle are pused in list as a type of string with some tab space #
            self.vehicles.append(vehicle)
            print ()
            print('This vehicle has been added, Thank you')
    def viewInventory(self):
        # vehicle are list here for managers #
        print('\t'.join(['ID','Brand', 'Model','Year', 'Color', 'Mileage',"Assigned BY", "Assigned"]))
        for idx, vehicle in enumerate(self.vehicles) :
            print(idx + 1, end='\t')
            print(vehicle)
    def viewuserInventory(self):
        # vehicle are list here for user because of _assigned based duration #
        try:
            print('\t'.join(['ID','Brand', 'Model','Year', 'Color', 'Mileage']))
            assigned = False
            total_index = []
            default_indx = 0
            for idx, vehicle in enumerate(self.vehicles) :
                hours_2_exceed = False
                if vehicle._assigned  != '':
                    date_time_obj = dm.datetime.strptime(vehicle._assigned, '%d-%m-%Y %H:%M:%S')
                    duration_in_s =  (dm.datetime.now() + timedelta(hours=5,minutes=30)) - date_time_obj
                    duration = duration_in_s.total_seconds() 
                    hours_2_exceed = divmod(duration, 60)[0]  >= self.default_assigned_duration
                if vehicle._assigned  == '' or hours_2_exceed:
                    assigned = True
                    default_indx += 1
                    print(default_indx, end='\t')
                    print('\t'.join(str(x) for x in [vehicle._brand, vehicle._model, vehicle._pub_year, vehicle._color, vehicle._mileage]))
                    total_index.append(idx)
            if not assigned: 
                print("\n Vehicle are too busy.Please try after some time")
            return total_index
        except Exception as e:
            logging.error("User check for Vehicle: " +str(e) )
            
            
class User(WareHouse):
    def __init__(self):
        # user and password  #
        self._user = {"manager":"manager","user":"user"}
        self.inventory =  WareHouse()

    # check for valid username and password otherwise return Invalid User #
    def check_user(self,name,password):
        try:
            if self._user[name] == password:
                return self._user[name]
            logging.info("Username:" + str(name) + ',' + "password : "+ str(password) )
            return "Invalid User"
        except:
            logging.info("Username:" + str(name) + ',' + "password : "+ str(password) )
            return "Invalid User"
    # Manager Action are performed here #
    def manager_role(self):
        
        while True:
            print()
            print('#1 Add Vehicle to WareHouse')
            print('#2 Delete Vehicle from WareHouse')
            print('#3 View Current WareHouse')
            print('#4 Update Vehicle in WareHouse')
            print('#5 Update Default assigned Duration')
            print('#6 Logout')
            userInput=input('Please choose from one of the above options: ') 
            inventory = self.inventory 
            if userInput=="1": 
                #add a vehicle
                inventory.addVehicle()
            elif userInput=='2':
                #delete a vehicle
                if len(inventory.vehicles) < 1:
                    print()
                    print('Sorry there are no vehicles currently in inventory')
                    print()
                    continue
                inventory.viewInventory()
                item = int(input('Please enter the number associated with the vehicle to be removed: '))
                if item   > len(inventory.vehicles):
                    print()
                    print('This is an invalid number')
                    print()
                else:
                    inventory.vehicles.remove(inventory.vehicles[item - 1])
                    print ()
                    print('This vehicle has been removed')
                    print()
            elif userInput == '3':
                #list all the vehicles
                if len(inventory.vehicles) < 1:
                    print()
                    print('Sorry there are no vehicles currently in inventory')
                    print()
                    continue
                inventory.viewInventory()
            elif userInput == '4':
                #edit vehicle 
                if len(inventory.vehicles) < 1:
                    print()
                    print('Sorry there are no vehicles currently in inventory')
                    print()
                    continue
                inventory.viewInventory()
                item = int(input('Please enter the number associated with the vehicle to be updated: '))
                if item   > len(inventory.vehicles):
                    print()
                    print('This is an invalid number')
                    print()
                else:
                    automobile = Cars()
                    if automobile.addVehicle() == True :
                        inventory.vehicles.remove(inventory.vehicles[item - 1])
                        inventory.vehicles.insert(item - 1, automobile)
                        print ()
                        print('This vehicle has been updated')
                        print()
            elif userInput == '5':
                userInput_assigned=input('Enter assigned minutes for vehicles: ')
                try:
                    userInput_assigned = int(userInput_assigned)
                    if userInput_assigned <=0:
                        print('\n Please enter valid Minutes greater then 0')
                        continue
                    inventory.default_assigned_duration =userInput_assigned
                    print(" \n Default assigned duration changed successfully" ) 
                except:
                    print('\n Please enter valid Minutes greater then 0')
                    continue
            elif userInput == '6':
                #exit the loop
                print()
                print('Thanks. You Can Come Back AT Any time.')
                print()
                break
            else:
                #invalid user input
                print()
                print('This is an invalid input. Please try again.')
                print()
        return True
    # user Action are performed here #
    def user_role(self):
        inventory  = self.inventory 
        total_index = []
        while True:
            
            print('#1 Assign Vehicle')
            print('#2 Logout')
            userInput=str(input('Please choose from one of the above options: '))
            if userInput=="1":
                if len(inventory.vehicles) < 1:
                    print()
                    print('Sorry there are no vehicles currently in inventory')
                    print()
                    continue
                total_index = inventory.viewuserInventory()
                if len(total_index)>0:
                    userInput_car=str(input('Please enter the number associated with the vehicle to Assign: '))
                    try:
                        inventory.vehicles[total_index[int(userInput_car)-1]]._assigned = dm.datetime.now(IST).strftime('%d-%m-%Y %H:%M:%S')
                        inventory.vehicles[total_index[int(userInput_car)-1]]._assigned_by = 'U'
                        print("Car Assigned Succefully Enjoy You ride")
                    except Exception as e:
                        logging.error("Getting Error while user tried for assign: " + str(e) )
                        print("\n This is an invalid input. Please try again with valid choice. \n")
                        continue
                
            elif userInput == "2":
                print()
                print("Thanks. You can Come Back Here At Any Time.")
                print()
                break
            else:
                print("\n This is an invalid input. Please try again with valid choice 1 or 2. \n")
        
    # These are base function which are triggered first #

    def login_base(self):
        while True:
            print('#1 Login')
            print('#2 Exit')
            userInput=str(input('Please choose from one of the above options: '))
            if userInput=="1":
                name = str(input('please enter Username: '))
                password = str(input('please enter password: '))
                # Validate user here#
                output = self.check_user(name,password)
                if output == "Invalid User":
                    print()
                    print("Invalid User Try again with valid credentials.")
                    print()
                    continue
                print()
                print(f"Successfully Logged In with {name.upper()}")
                # Role base sub function called here #
                if name == 'manager':
                    self.manager_role()
                    continue
                else:
                    self.user_role()
                    continue
            elif userInput=="2":
                print()
                print("Logout successfully.")
                print()
                break
            else:
                print()
                print("This is an invalid input. Please try again with valid choice 1 or 2.")
                print()
                
user = User()
user.login_base()